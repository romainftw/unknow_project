---
title: "MagicRequest"
description: "MagicRequest est une brique mutualisé permettant la mise à disposition de flux de données (inetrnes/externes) templatisés."
featured_image: ' '
menu:
  main:
    weight: 1
---
### Referent : Sofiene GALLALOU 

:smirk_cat:[Repo gitlab ](https://gitlab.si.francetelecom.fr/enablers/magicrequest)


Des urls publiques et privées sont utilisées :
 
En Prod :
- https://magicrequest.orange.fr pour les externes
- https://magicrequest.prod.pub.cfy.multis.p.fti.net pour les internes

Hors prod :
- https://magicrequest.cf.staging.orange.fr  pour les externes
- https://magicrequest.staging.pub.cfy.multis.p.fti.net  pour les internes


* [BO de prod](https://magicrequest-bo.orangeportails.net/)
* [BO de preprod](https://magicrequest-bo.staging.orangeportails.net)

* [Dashboard](https://loghosts.orangeportails.net/kibana6/shm/app/kibana#/dashboard/f9801e40-0281-11e8-ae5b-e3b38a8cb1e2?_g=h@44136fa&_a=h@922adbb)

Sondes
* [partner contentes en page](https://monitoring-factory-ovv.orangeportails.net/?searchItem=s_mrequest_prod_api_partner_contentes&showSilent=true)

* [Doc dédiée sonde partner contentes page](https://hebex-wiki.orangeportails.net/index.php?title=Pfs_/service_s_mrequest_prod_api_partner_contentes/Sonde_watchP)

* [Sondes partie crons](https://monitoring-factory-ovv.orangeportails.net/?tag=HBX%3A%5BS%5D+Magic+Request&hiddenStates=NO_DATA)
