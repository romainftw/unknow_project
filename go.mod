module gitlab.com/pages/hugo

go 1.21

require (
	github.com/gethyas/doks v1.1.1 // indirect
	github.com/theNewDynamic/gohugo-theme-ananke v0.0.0-20231122160523-91df000ca827 // indirect
)
